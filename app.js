const express = require('express')
const app = express()
const port = 3000

/**
 * This function adds two numbers together
 * @param {Number} a first param
 * @param {Number} b second param
 * @return {Number}
 */
const add = (a,b) => {
    return a + b; 
}
/** Summary */
app.get('/add/', (req, res) => {    
   const x = add(1,2);
   res.send(`Sum: ${x}`);
  })

/** Welcome message */
app.get('/', (req, res) => {  
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`listening at http://localhost:${port}`)
})